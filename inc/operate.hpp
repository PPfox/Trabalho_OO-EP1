#ifndef _OPERATE_H
#define _OPERATE_H

#include <iostream>

using namespace std;

class operate{
	private:
		byte tag;
		int SoC;
	public:
		operate();

		static void operador(byte tag,int fd);
		int conection();

		void setTag(byte tag);
		byte getTag();

		void setSoC(int SoC);
		int getSoC();

};
#endif
		