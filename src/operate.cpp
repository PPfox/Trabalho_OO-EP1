#include <iostream>

#include "array.hpp"
#include "network.hpp"
#include "operate.hpp"
#include "packet.hpp"

#define server "45.55.185.4"
#define door_operate 3000
#define door_eco 3001

#define C0 0xc0
#define C1 0xc1
#define C2 0xc2
#define C3 0xc3
#define A0 0xa0
#define A1 0xa1
#define A2 0xa2
#define A4 0xa3
#define A5 0xa5
#define A6 0xa6
#define B0 0xb0
#define B1 0xb1

using namespace std;
operate::operate(){
    this->SoC = 0;
    this->tag = 0;
}
// void operate::setTag(byte tag){
// 	this->tag = tag;
// }

int operate::conection(){
	cout<<"Iniciando a comunicação com o servidor..."<<endl;
	if ((this->SoC = network::connect(server, door_operate)) < 0){
		cout<<this->SoC<<endl;
		return this->SoC;
	}
	else{
		cout<<"Conexão feita com sucesso: ";
		cout<<this->SoC<<endl;
		return this->SoC;
			printf("\n");
	}
	return this->SoC;
}
int operate::getSoC(){
	return this->SoC;
}
void operate::operador(byte tag,int fd){
	cout<<"teste: "<<fd<<endl;
	byte id [] = {0x31, 0xc4, 0xe8, 0xca, 0x57, 0x1f, 0xc8, 0xb3};
	array::array* pacote_enviar = array::create(7);
	array::array* pacote_recebe;
	array::array* ID = array::create(8,id);
	switch (tag){
		case C0: //Iniciando Registro: envia c0 e recebe c1, caso esteja correto
			 cout<<"Iniciando protocolo de registro..."<<endl;

			 pacote_enviar->data[0]=0x03;
			 pacote_enviar->data[1]=0;
			 pacote_enviar->data[2]=0;
			 pacote_enviar->data[3]=0;
			 pacote_enviar->data[4]=0xc0;
			 pacote_enviar->data[5]=0;
			 pacote_enviar->data[6]=0;

			cout<<"Pacote a ser enviado["<<pacote_enviar->length<<"]: "<<endl;
			for (int i=0;i<((int) pacote_enviar->length);i++){
				printf("%x ",pacote_enviar->data[i]);
			}
			cout<<endl;
			network::write(fd, pacote_enviar);
				
			 if ((pacote_recebe = network::read(fd)) == nullptr){
		 		 cout<<"Leitura NULA";
				 fd = -3;
			 }
			 printf("\n\n");

		array::destroy(pacote_enviar);
		break;

		case C2:	//Enviando ID C2 e recebendo chave simetrica encriptografada C3

			RSA *key = crypto::rsa_read_public_key_from_PEM("server_pk.pem");
			array::array *encrypt_ID = crypto::rsa_encrypt(ID,key);	

			cout<<"0xc2\nPedindo registro..."<<endl;
			// cout<<"\nID["<<(int) ID->length<<"]:";
			// for (int i=0;i<((int) ID->length);i++){
			// 	printf("%x ",ID->data[i]);
			// }
			// cout<<"\n\nID encriptografado["<<(int) encrypt_ID->length<<"]: "<<endl;
			// for (int i=0;i<((int) encrypt_ID->length);i++){
			// 	printf("%x ",encrypt_ID->data[i]);
			// }
			printf("\n\n");
			array::array *pacote = array::create(27+(int) encrypt_ID->length);
			array::array *valor_dados = array::create((int) encrypt_ID->length);

			memcpy(valor_dados->data,encrypt_ID,(int) encrypt_ID->length);
			// cout<<"\n\nvalor_dados:"<<endl;
						
			// cout<<endl;
			pacote->data[0] = 0xC2;
			pacote->data[1] = 0x00;
			pacote->data[2] = 0x02;
			memcpy(pacote->data + 3, encrypt_ID->data, (int) encrypt_ID->length);

			array::array* hash = crypto::sha1(encrypt_ID);

			memcpy(pacote->data+3+(int) encrypt_ID->length, hash->data, 20);

			array::array* pacote_enviar = array::create(27+(int) encrypt_ID->length);
			pacote_enviar->data[0]=23;
			pacote_enviar->data[1]=2;
			pacote_enviar->data[2]=0;
			pacote_enviar->data[3]=0;
						
			memcpy(pacote_enviar-> data+4, pacote->data,27+(int) encrypt_ID->length);

			array::destroy(pacote);
			array::destroy(hash);
			array::destroy(valor_dados);

			cout<<"Pacote a ser enviado["<<pacote_enviar->length<<"]: "<<endl;
			for (int i=0;i<((int) pacote_enviar->length);i++){
				printf("%x ",pacote_enviar->data[i]);
			}
			cout<<endl;
		
				network::write(fd, pacote_enviar);
				if ((pacote_recebe = network::read(fd)) == nullptr){
					cout<<"leitura NULA";
					fd = 0;
				}
				else{
					fd = 1;
					cout<<"Imprimindo conteudo do pacote recebido ["<<pacote_recebe->length<<"]:"<<endl;
					for (int i=0;i<((int) pacote_recebe->length);i++){
						printf("%x ",pacote_recebe->data[i]);
					}
				}
				printf("\n\n\n");

		 	array::array *SKey = array::create((int) encrypt_ID->length);
		 	memcpy(SKey->data+7,pacote_recebe->data,(int) encrypt_ID->length);
			cout<<"Imprimindo chave paralela ["<<SKey->length<<"]:"<<endl;
			for (int i=0;i<((int) SKey->length);i++){
				printf("%x ",SKey->data[i]);
			}
						
			array::destroy(pacote_enviar);
			
		break;
		//  case A0: //Inicia Autenticação com o servidor: envia A0 e recebe A1
				
		// break;
		// case A2: 	//Pede desafio para confirmar autencição: envia A2 e recebe A4(desafio)
		// 	
		// 	break;
		// case A5:	//Envia desafio desencriptografado: Envia A5 e recebe A6(desafio resolvido)
		// 	
		// 	break;
		// case B1:	//Pede Objeto referente ao ID e recebe: Envia B1 e recebe B2(imagem)
		// 	
		// 	break;
		}
}